import React, {useState, useEffect } from 'react';
import logo from './logo.svg';
import Layout from './layout/latyoutComponent';
import './App.css';

function App() {
	return (
		<div className="App">
			<Layout />
		</div>
	);
}

export default App;
