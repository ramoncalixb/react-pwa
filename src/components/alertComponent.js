import React, { useState } from 'react';
import { Toast } from 'react-bootstrap';

function AlertComponent() {
	const [ showB, setShowB ] = useState(false);
	const toggleShowB = () => setShowB(!showB);
	window.addEventListener('load', () => {
		function handleNetworkChange(event) {
			if (navigator.onLine) {
				console.log('online');
				document.body.classList.remove('offline');
			} else {
				console.log('off');
				setShowB(true);
			}
		}
		window.addEventListener('online', handleNetworkChange);
		window.addEventListener('offline', handleNetworkChange);
	});

	return (
		<div
			aria-live="polite"
			aria-atomic="true"
			style={{
				position: 'relative',
				minHeight: '100px'
			}}
		>
			<Toast
				style={{
					position: 'absolute',
					top: 0,
					right: 0
				}}
				onClose={toggleShowB}
				show={showB}
				animation={false}
				onClose={toggleShowB}
				show={showB}
				animation={false}
			>
				<Toast.Header>
					<img src="holder.js/20x20?text=%20" className="rounded mr-2" alt="" />
					<strong className="mr-auto">offline mode</strong>
				</Toast.Header>
				<Toast.Body>Please check your internet conection</Toast.Body>
			</Toast>
		</div>
	);
}

export default AlertComponent;
