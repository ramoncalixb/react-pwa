import React, {useState, useEffect} from 'react';
import { Form } from 'react-formio';
import { Button, Card } from 'react-bootstrap';
import { withRouter, useLocation } from 'react-router'

function Formio() {
    let location = useLocation()
    const [stateForm, setStateForm] = useState()
    const [form, setForm] = useState()
    const onChange = (meta) => {
        setForm(meta.data);
    };
    useEffect(() => {
        if (location.state) {
            setStateForm(location.state.url)
        }
    })
    const onSubmit = () => {
        console.log(form)
    }
    return (
        <>
        
		<Form
			src={stateForm}
			onChange={onChange}
            />
            <Button variant="primary"  size="lg" onClick={onSubmit} block>Submit </Button>
            </>
	);
}

export default Formio;
