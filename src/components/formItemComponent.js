
import React, {useState} from 'react';
import { Form } from 'react-formio';
import { Button, Card, ListGroup } from 'react-bootstrap';
import { useHistory } from "react-router-dom";


function FormItem(data) {

    let history = useHistory();
    let {caseCategory, caseSubCategory, serviceFormRenderURL} = data.data;

    const handleClick = () => {

        history.push({pathname: "/form", state: {
            url: serviceFormRenderURL, 
        }
     })
    }
    return (
        <div className="col-sm-6" style={{paddingBottom: "15px"}}>
        <div className="card">
        <div className="card-body" style={{textAlign: 'left', padding: '15px'}}>
            <h3 className="card-title">{caseCategory}</h3>
            <h5 className="card-title">{caseSubCategory}</h5>
            <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <a href="#" className="btn btn-danger" onClick={handleClick}>Continuar</a>
        </div>
    </div>
</div>
    )
}

export default FormItem