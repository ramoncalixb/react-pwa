import React, {useState} from 'react';
import { Form } from 'react-formio';
import { Button, Card, ListGroup } from 'react-bootstrap';
import FormItem from './formItemComponent'

const mockdata = [
    {
        "_id": "5d9a5c8de0f53d4900789e11",
        "isActive": true,
        "caseType": "Service",
        "caseCategory": "Internships and International Office",
        "caseSubCategory": "Internship Start Process/Extension",
        "serviceCatalogGUID": "b3f667ed-7f3a-e911-a95d-000d3ac1b17a",
        "InstitutionId": "FMU",
        "serviceName": "Activity Plan Delivery for Internship",
        "servicePtName": "Entrega do Plano de Atividades de Estágio",
        "financialHoldRestriction": false,
        "financialHoldCodes": [],
        "nonFinancialHoldCodes": [
            "A2",
            "A4",
            "A5"
        ],
        "feeRequired": false,
        "isPrepay": false,
        "initialCaseStatus": "New",
        "bannerDetailCode": "",
        "baseFee": 0,
        "startDate": "2001-01-01",
        "endDate": "9999-12-31",
        "currency": "BRL",
        "deliveryType": [],
        "nonFinancialHoldRestriction": true,
        "priorityFee": 0,
        "enablePriority": false,
        "autoDocGen": false,
        "isCaseNeeded": true,
        "serviceFormRenderURL": "https://xchnmqeppecsmnr.form.io/entregadoplanodeatividadesdeestagio",
        "informativeTextRenderURL": ""
    },
    {
        "_id": "5d9a6361dd13c9317c739945",
        "isActive": true,
        "caseType": "Service",
        "caseCategory": "Academic Requests",
        "caseSubCategory": "Change Requests",
        "serviceCatalogGUID": "61cb55e5-a09e-e811-a96e-000d3ac1bf27",
        "InstitutionId": "FMU",
        "serviceName": "Program Change",
        "servicePtName": "Transferência de Campus",
        "financialHoldRestriction": true,
        "financialHoldCodes": [
            "F1",
            "F2"
        ],
        "nonFinancialHoldCodes": [
            "A2",
            "A4",
            "A5"
        ],
        "feeRequired": false,
        "isPrepay": false,
        "initialCaseStatus": "New",
        "bannerDetailCode": "",
        "baseFee": 0,
        "startDate": "2019-03-15",
        "endDate": "2020-04-30",
        "currency": "BRL",
        "deliveryType": [],
        "nonFinancialHoldRestriction": true,
        "priorityFee": 0,
        "enablePriority": false,
        "autoDocGen": false,
        "isCaseNeeded": true,
        "serviceFormRenderURL": "https://xchnmqeppecsmnr.form.io/campuschange",
        "informativeTextRenderURL": ""
    },
    {
        "_id": "5d9a6586e0f53d4900789e24",
        "isActive": true,
        "caseType": "Service",
        "caseCategory": "Enrollment and Reenrollment",
        "caseSubCategory": "Reenrollment",
        "serviceCatalogGUID": "6c7ca4e9-b43a-e911-a956-000d3ac1b6c7",
        "InstitutionId": "FMU",
        "serviceName": "Reenrollment",
        "servicePtName": "Rematrícula",
        "financialHoldRestriction": true,
        "financialHoldCodes": [
            "F1",
            "F2"
        ],
        "nonFinancialHoldCodes": "A3,A4,A5",
        "feeRequired": false,
        "isPrepay": false,
        "initialCaseStatus": "",
        "bannerDetailCode": "",
        "baseFee": 0,
        "startDate": "2001-01-01",
        "endDate": "9999-12-31",
        "currency": "BRL",
        "deliveryType": [],
        "nonFinancialHoldRestriction": true,
        "priorityFee": 0,
        "enablePriority": false,
        "autoDocGen": false,
        "isCaseNeeded": false,
        "serviceFormRenderURL": "https://xchnmqeppecsmnr.form.io/rematriculapart1",
        "informativeTextRenderURL": ""
    }
]
function FormList(props) {
    console.log(props)
    const [form, setForm] = useState()
    let content = [];
     mockdata.map((form, i) => {
         if ((i +1 ) % 2 == 0) {
             content.push(
                <FormItem data={form}/>
             )
         } else {
             content.push(
                <FormItem data={form}/>
             )
         }
     })  
  
    return (
        <>
        <div>
            {content}
        </div>            
        </>
	);
}

export default FormList;
