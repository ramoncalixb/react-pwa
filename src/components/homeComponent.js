import React from 'react';
import { Jumbotron, Button, Card } from 'react-bootstrap';
import Tabs from '../layout/tabsComponent';
import Alert from './alertComponent';
function Home() {
	return (
		<div>
            <Tabs />
            <Alert />
		</div>
	);
}

export default Home;
