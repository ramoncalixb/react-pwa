import React from 'react';
import { Button, Navbar, Nav, NavDropdown, Jumbotron } from 'react-bootstrap';
import Home from '../components/homeComponent'

function Layout() {
    return (
        <>
		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
			<Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
			<Navbar.Toggle aria-controls="responsive-navbar-nav" />
			<Navbar.Collapse id="responsive-navbar-nav">
				<Nav className="mr-auto">
				</Nav>
				<Nav>
					<Nav.Link href="#deets">More deets</Nav.Link>
					<Nav.Link eventKey={2} href="#memes">
						Dank memes
					</Nav.Link>
				</Nav>
			</Navbar.Collapse>
            </Navbar>
            <Home/>
        </>
	);
}

export default Layout;
