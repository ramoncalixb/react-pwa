import React, { useState } from 'react';
import { Nav, Card } from 'react-bootstrap';
import Formio from '../components/formComponent';
import FormList from '../components/formListComponent';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useRouteMatch
  } from "react-router-dom";

function Tabs() {
	const [ currentComponent, setCurrentComponent ] = useState(<Formio />);

	const handleSelect = (eventKey) => {
		switch (eventKey) {
			case '1':
				setCurrentComponent(<Formio />);
				break;
			case '2':
				setCurrentComponent(<FormList/>);
				break;
		}
	};

	return (
        <Router>
            <div>

            <Nav variant="tabs" onSelect={handleSelect}>
            <Nav.Item>
                <Nav.Link >
                <Link to="/">List of Forms</Link></Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link > <Link to="/form">Form Io</Link></Nav.Link>
            </Nav.Item>
        </Nav>
          <Switch>
            <Route exact path="/">
			<br />
			<div className="container ">
				<div className="row">
					<Card style={{ width: '100rem' }}>
						<Card.Body><FormList /></Card.Body>
					</Card>
				</div>
			</div>   
            </Route>
            <Route path="/form">
            <br />
			<div className="container ">
				<div className="row">
					<Card style={{ width: '100rem' }}>
						<Card.Body><Formio /></Card.Body>
					</Card>
				</div>
			</div>
             </Route>
          </Switch>
        </div>
      </Router>
	);
}

export default Tabs;
